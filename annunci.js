            // Navbar scroll //

    let navbar = document.querySelector('#navbar');
    let logoCassa = document.querySelector('#logoCassa');
    let colorlinknav = document.querySelectorAll('.colorlinknav');
    let navIcon = document.querySelector ('#navIcon');
    let navLinks = document.querySelectorAll('.nav-link');
    let navCollapse = document.querySelector('#navCollapse');
    


   document.addEventListener('scroll' , () =>{
      
       let scrolled = Math.round(window.scrollY);

      if (scrolled>0){ 
                navbar.style.minHeight = '80px';
                navbar.style.backgroundColor = 'var(--purple)';
                navCollapse.style.backgroundColor = 'var(--purple)';
                logoCassa.src = './media/logonero.png';

                navLinks.forEach( (link) => {
                   link.classList.remove('text-w-link');
                   link.classList.add('text-b-link');
               })
               navIcon.classList.remove('text-w');
               navIcon.classList.add('text-b');
       
                
       
                
      } 
      else { navbar.style.minHeight = '150px';
             navbar.style.backgroundColor = 'var(--b)';
             logoCassa.src = './media/logobianco.png';
             
             navLinks.forEach( (link) => {
               link.classList.add('text-w-link');
               link.classList.remove('text-b-link');
           })
           
           navIcon.classList.add('text-w');
           navIcon.classList.remove('text-b');
       }     
             
      })


    //   Fine Navbar Scroll

   

fetch('./annunci.json').then( (response)=> response.json() ).then( (data) => {


          // console.log(data);
          
              function setCategoryButtons(){
          
                  let categories = Array.from(new Set(data.map(  (el) => el.category )));
                  let radioWrapper = document.querySelector('#radioWrapper');
                  categories.forEach( (category) => {
                      let div = document.createElement('div');
                      div.classList.add('form-check');
                      div.innerHTML = `
                      
                          <input class="form-check-input" type="radio" name="categories" id="${category}">
                          <label class="form-check-label text-purple" for="${category}">
                              ${category}
                          </label>
                      
                      `
                      radioWrapper.appendChild(div);
                  } )
          
              }
          
              setCategoryButtons()
          
          
              function truncateString(str){
                  if(str.length > 10){
                      return str.split(' ')[0] + '...'
                  } else{
                      return str
                  }
              }
          
          
          
              function showCards(array){
                  let cardsWrapper = document.querySelector('#cardsWrapper');
          
                  cardsWrapper.innerHTML = '';
          
                  array.sort( (a,b) => Number(a.price) - Number(b.price) );
          
                  array.forEach( (annuncio)=> {
          
                      let div = document.createElement('div');
                      div.classList.add('card-ads-custom');
                      div.innerHTML = `
                      
                      <p class="h3" title="${annuncio.name}">${truncateString(annuncio.name)}</p>
                       
                      <p class="h4">${annuncio.category}</p>
          
                      <p class="lead">${annuncio.price} $</p>
          
                      <div class="d-flex justify-content-end">
          
                          <i class="far fa-heart fa-2x"></i>
                      </div>
                      
                      `
          
                      cardsWrapper.appendChild(div)
          
                  })
          
                  let hearts = document.querySelectorAll('.fa-heart');
          
                
          
                  hearts.forEach( (cuore) => {
                      
                    
                          cuore.addEventListener('click' , ()=>{
                              cuore.classList.toggle('far');
                              cuore.classList.toggle('fas');
                              cuore.classList.toggle('scale');
                          })
                      
          
                  } )
          
          
          
          
              }
          
              showCards(data)
          
          
              function filterByCategory(array){
                 
                  let categoria = Array.from(radioButtons).find(  (button) => button.checked ).id
          
                  if(categoria != 'All'){
                      let filtered = array.filter( (el)=> el.category == categoria  )
                      console.log(filtered);
                      return filtered;
                  } else {
                      return array;
                  }
              }
          
             
              let priceInput = document.querySelector('#priceInput');
          
          
              function setPriceInput(){
                  let priceSpan = document.querySelector('#priceSpan');
                  let maxPrice = Math.ceil(data.map( (el) => +el.price ).pop())
                  priceInput.setAttribute('max' , maxPrice);
                  priceInput.value = maxPrice;
                  priceInput.addEventListener('input' , ()=> {
                      priceSpan.innerHTML = `${priceInput.value} $`
                  })
              }
          
              setPriceInput()
          
              function filterByPrice(array){
                  
                      let filtered = array.filter( (el)=> +el.price <= priceInput.value );
                      return filtered;
               
              }
          
            
          
              function filterByWord(array){
                 
                      let filtered = array.filter( (el)=> el.name.toLowerCase().includes(wordInput.value.toLowerCase())  );
                      return filtered;
                
              }
          
          
          
              function globalFilter(){
                  let filteredByCategory = filterByCategory(data); 
                  let filteredByPrice = filterByPrice(filteredByCategory);
                  let filteredByWord = filterByWord(filteredByPrice);
          
                  showCards(filteredByWord)
              }
          
            
          
              let radioButtons = document.querySelectorAll('.form-check-input');
              let wordInput = document.querySelector('#wordInput');
          
              radioButtons.forEach( (button) => {
                  button.addEventListener('click' , ()=>{
                 
                      globalFilter();
                  })
              })
          
              priceInput.addEventListener('input', ()=>{
                  globalFilter();
              })
          
              wordInput.addEventListener('input', ()=>{
                  globalFilter();
              })
          
          } )
          
