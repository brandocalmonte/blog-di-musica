
        // Navbar scroll //

        let navbar = document.querySelector('#navbar');
        let logoCassa = document.querySelector('#logoCassa');
        let colorlinknav = document.querySelectorAll('.colorlinknav');
        let navIcon = document.querySelector ('#navIcon');
        let navLinks = document.querySelectorAll('.nav-link');
        let navCollapse = document.querySelector('#navCollapse');
        
    
    
       document.addEventListener('scroll' , () =>{
          
           let scrolled = Math.round(window.scrollY);
    
          if (scrolled>0){ 
                    navbar.style.minHeight = '80px';
                    navbar.style.backgroundColor = 'var(--purple)';
                    navCollapse.style.backgroundColor = 'var(--purple)';
                    logoCassa.src = './media/logonero.png';
    
                    navLinks.forEach( (link) => {
                       link.classList.remove('text-w-link');
                       link.classList.add('text-b-link');
                   })
                   navIcon.classList.remove('text-w');
                   navIcon.classList.add('text-b');
           
                    
           
                    
          } 
          else { navbar.style.minHeight = '150px';
                 navbar.style.backgroundColor = 'var(--b)';
                 logoCassa.src = './media/logobianco.png';
                 
                 navLinks.forEach( (link) => {
                   link.classList.add('text-w-link');
                   link.classList.remove('text-b-link');
               })
               
               navIcon.classList.add('text-w');
               navIcon.classList.remove('text-b');
           }     
                 
          })
    
    
        //   Fine Navbar Scroll






let teachers = [
          {name : 'Heinz Wesener', languages : ['Brand Manager'], url : './media/worker3.jpg'},
          {name : 'Mike Preston', languages : ['Marketing and sales'], url : './media/worker2.jpg'},
          {name : 'Rita Howkings', languages : ['Mixing & Mastering'], url : './media/worker1.jpg'},
          {name : 'Sophie Larkin', languages : ['Human Resources'], url : './media/worker5.jpg'},
          ]
          
          let circle = document.querySelector('.circle');
          
          teachers.forEach( (docente) =>{
                    
                    let div = document.createElement('div');
                    div.classList.add('moved');
                    div.style.backgroundImage = `url(${docente.url})`
                    
                    circle.appendChild(div)
                    
          }  )
          
          
          
          let opener = document.querySelector('.opener');
          let movedIcons = document.querySelectorAll('.moved');
          let closed = document.querySelector('#closed');
          let opened = document.querySelector('#opened');
          let confirm = false;
          let cardWrapper = document.querySelector('#cardWrapper')
          
          opener.addEventListener('click' , ()=>{
                    if(confirm == false){
                              movedIcons.forEach( (icon, i) =>{
                                        let angle = (360 * i) / movedIcons.length
                                        
                                        icon.setAttribute('data-angle' , angle)
                                        
                                        icon.style.transform = `rotate(-${angle}deg)   translate(150px)   rotate(${angle}deg)  `
                                        
                              }  )
                              closed.classList.toggle('d-none');
                              opened.classList.toggle('d-none');
                              confirm = true;
                    } else {
                              
                              cardWrapper.innerHTML = ''
                              movedIcons.forEach( (icon, i) =>{
                                        let angle = (360 * i) / movedIcons.length
                                        
                                        icon.style.transform = `rotate(0deg)   translate(0px)   rotate(0deg)  `
                                        
                                        
                                        
                              }  )
                              closed.classList.toggle('d-none');
                              opened.classList.toggle('d-none');
                              confirm = false;
                              
                    }
          })
          
          
          
          movedIcons.forEach( (icon, i) =>{
                    
                    icon.addEventListener('click' , ()=>{
                              
                              cardWrapper.innerHTML = ''
                              
                              let angle = +icon.dataset.angle;
                              
                              let exp = 360 - angle;
                              
                              movedIcons.forEach( (moved) => {
                                        let movedAngle = +moved.dataset.angle;
                                        
                                        moved.style.transform = `rotate(-${exp + movedAngle}deg) translate(150px) rotate(${exp + movedAngle}deg)`
                              })
                              
                              let div = document.createElement('div');
                              div.classList.add('card');
                              div.style.width = '300px'
                              div.innerHTML = `
                              
                              <img src="${teachers[i].url}" class="card-img-top" alt="immagini team" >
                              <div class="card-body bg-b text-purple" >
                                        <h5 class="card-title text-purple">${teachers[i].name}</h5>
                                        <p class="card-text text-purple">${teachers[i].languages}</p>
                                        
                              </div>
                              
                              `
                              
                              cardWrapper.appendChild(div);
                              
                    })
                    
          } )