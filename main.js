        // Hover

        let hoverCards = document.querySelectorAll('.hover-card');
        let hoverIcons = document.querySelectorAll('.hover-icon');
        
        let incrementNumber = document.querySelector('#incrementNumber');
        let incrementNumber2 = document.querySelector('#incrementNumber2');
        let incrementNumber3 = document.querySelector('#incrementNumber3');

        
        
        hoverCards.forEach( (column, i) => {
        
            column.addEventListener('mouseenter' , ()=> {
                hoverIcons[i].classList.remove('text-purple');
                hoverIcons[i].classList.add('text-b');
        
                switch (i) {
                    case 0:
                        incrementNumber.style.color = 'var(--b)';
                        
                        break;
        
                    case 1:
                        incrementNumber2.style.color = 'var(--b)';
                        break;
                    default:
                        incrementNumber3.style.color = 'var(--b)';
                        break;
                }
            })
        
            column.addEventListener('mouseleave' , ()=> {
                hoverIcons[i].classList.add('text-purple');
                hoverIcons[i].classList.remove('text-b');
                incrementNumber.style.color = 'var(--purple)';
                incrementNumber2.style.color = 'var(--purple)';
                incrementNumber3.style.color = 'var(--purple)';
        
            })
        
        })
        
        
        
        function incrementNumberFunction(number, target){
        
            let counter = 0;
        
            setTimeout(() => {
                let interval = setInterval(() => {
                
                    if(counter < number){
                        counter++;
                        target.innerHTML = counter;
                    } else {
                        clearInterval(interval)
                    }
            
                }, 1);
        
        
            }, 1000);
             
        }



        //         Navbar scroll //

     let navbar = document.querySelector('#navbar');
     let logoCassa = document.querySelector('#logoCassa');
     let colorlinknav = document.querySelectorAll('.colorlinknav');
     let navIcon = document.querySelector ('#navIcon');
     let navLinks = document.querySelectorAll('.nav-link');
     let navCollapse = document.querySelector('#navCollapse');
     


    document.addEventListener('scroll' , () =>{
       
        let scrolled = Math.round(window.scrollY);

       if (scrolled>0){ 
                 navbar.style.minHeight = '80px';
                 navbar.style.backgroundColor = 'var(--purple)';
                 navCollapse.style.backgroundColor = 'var(--purple)';
                 logoCassa.src = './media/logonero.png';

                 navLinks.forEach( (link) => {
                    link.classList.remove('text-w-link');
                    link.classList.add('text-b-link');
                })
                navIcon.classList.remove('text-w');
                navIcon.classList.add('text-b');
        
                 
        
                 
       } 
       else { navbar.style.minHeight = '150px';
              navbar.style.backgroundColor = 'var(--b)';
              logoCassa.src = './media/logobianco.png';
              
              navLinks.forEach( (link) => {
                link.classList.add('text-w-link');
                link.classList.remove('text-b-link');
            })
            
            navIcon.classList.add('text-w');
            navIcon.classList.remove('text-b');
        }     
              
       })


    //    Fine Navbar Scroll



        // IntersectionObserver
        // setTimeout
          
        let confirm = false;
        
        let observer = new IntersectionObserver( (entries) => {
        
            entries.forEach((entry) => {
                if(entry.isIntersecting && confirm == false){
                    incrementNumberFunction(499, incrementNumber );
                    incrementNumberFunction(676, incrementNumber2 );
                    incrementNumberFunction(1221, incrementNumber3 );
                    confirm = true;
                }
            })
        
        })
        
        observer.observe(incrementNumber);

        // swiper//

        
        
        let swiperWrapper = document.querySelector('.swiper-wrapper');

let advices = [
    {name: 'Mark' , description: "Highly recommended", voto: 5},
    {name: 'Robert' , description: "High quality service", voto: 4},
    {name: 'Steve' , description: "Great products", voto: 3},
    {name: 'Luis' , description: "Solid package", voto: 5}
]

advices.forEach( (annuncio) => {

    let div = document.createElement('div');
    div.classList.add('swiper-slide' , 'd-flex', 'justify-content-center', 'align-items-center');
    div.innerHTML = `
    
    <div class="card-custom">
        <div class="d-flex justify-content-end p-3 starDiv">
        
        </div>
        <p class="text-w fst-italic fs-30 mt-3">"${annuncio.description}"</p>
        <p class="text-purple text-center">${annuncio.name}</p>
    </div>
    `

    swiperWrapper.appendChild(div);

} )

let starDiv = document.querySelectorAll('.starDiv');

advices.forEach( (annuncio, i) => {

  
        for(let j = 1; j <= annuncio.voto; j++){
            let icon = document.createElement('i');
            icon.classList.add('fas', 'fa-star', 'text-purple')
            starDiv[i].appendChild(icon)
        }

    if(annuncio.voto < 5) {
 
        let difference = 5 - annuncio.voto

        for(let j = 1; j <= difference; j++){
            let icon = document.createElement('i');
            icon.classList.add('far', 'fa-star', 'text-w')
            starDiv[i].appendChild(icon)
        }
    }


} )
